package io.jplus.admin.service;

import com.jfinal.plugin.activerecord.Page;
import io.jboot.db.model.Columns;
import io.jplus.admin.model.UserRole;

import java.util.List;

public interface UserRoleService  {

    /**
     * find model by primary key
     *
     * @param id
     * @return
     */
    public UserRole findById(Object id);


    /**
     * find all model
     *
     * @return all <UserRole
     */
    public List<UserRole> findAll();


    /**
     * delete model by primary key
     *
     * @param id
     * @return success
     */
    public boolean deleteById(Object id);


    /**
     * delete model
     *
     * @param model
     * @return
     */
    public boolean delete(UserRole model);


    /**
     * save model to database
     *
     * @param model
     * @return id value if save success
     */
    public Object save(UserRole model);


    /**
     * save or update model
     *
     * @param model
     * @return id value if save or update success
     */
    public Object saveOrUpdate(UserRole model);


    /**
     * update data model
     *
     * @param model
     * @return
     */
    public boolean update(UserRole model);


    /**
     * page query
     *
     * @param page
     * @param pageSize
     * @return page data
     */
    public Page<UserRole> paginate(int page, int pageSize);


    /**
     * page query by columns
     *
     * @param page
     * @param pageSize
     * @param columns
     * @return page data
     */
    public Page<UserRole> paginateByColumns(int page, int pageSize, Columns columns);


    /**
     * page query by columns
     *
     * @param page
     * @param pageSize
     * @param columns
     * @param orderBy
     * @return page data
     */
    public Page<UserRole> paginateByColumns(int page, int pageSize, Columns columns, String orderBy);

    public boolean deleteByUserId(String userId);

    public List<UserRole> findListByUserId(String userId);

    public boolean deleteById(Object... ids);
}