package io.jplus.admin.service.impl;

import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Page;
import io.jboot.aop.annotation.Bean;
import io.jboot.components.rpc.annotation.RPCBean;
import io.jboot.db.model.Columns;
import io.jboot.service.JbootServiceBase;
import io.jplus.admin.model.Oss;
import io.jplus.admin.service.OssService;
import io.jplus.common.Query;


@Bean
@RPCBean
public class OssServiceImpl extends JbootServiceBase<Oss> implements OssService {

    @Override
    public boolean deleteById(Object... ids) {
        boolean tag = false;
        for (Object id : ids) {
            tag = DAO.deleteById(id);
        }
        return tag;
    }

    @Override
    public Page<Oss> queryPage(Query query) {
        Columns columns = Columns.create();
        String name = (String) query.get("name");
        if (StrKit.notBlank(name)) {
            columns.like("name", "%" + name + "%");
        }
        return DAO.paginateByColumns(query.getCurrPage(), query.getLimit(), columns, query.getOrderBy());
    }

}