package io.jplus.admin.service.impl;

import com.jfinal.aop.Inject;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.SqlPara;
import io.jboot.aop.annotation.Bean;
import io.jboot.components.rpc.annotation.RPCBean;
import io.jboot.db.model.Column;
import io.jboot.db.model.Columns;
import io.jboot.service.JbootServiceBase;
import io.jplus.admin.model.Dept;
import io.jplus.admin.model.Role;
import io.jplus.admin.model.RoleDept;
import io.jplus.admin.model.RoleMenu;
import io.jplus.admin.service.DeptService;
import io.jplus.admin.service.RoleDeptService;
import io.jplus.admin.service.RoleMenuService;
import io.jplus.admin.service.RoleService;
import io.jplus.common.Query;

import java.util.ArrayList;
import java.util.List;


@Bean
@RPCBean
public class RoleServiceImpl extends JbootServiceBase<Role> implements RoleService {

    @Inject
    DeptService deptService;
    @Inject
    RoleMenuService roleMenuService;
    @Inject
    RoleDeptService roleDeptService;

    @Override
    public List<Role> findByColumn(Column column) {
        return null;
    }

    @Override
    public List<Role> findByUserId(Long userId) {
        SqlPara sqlPara = Db.getSqlPara("admin-role.findByUserId");
        sqlPara.addPara(userId);
        return DAO.find(sqlPara);
    }

    @Override
    public Page<Role> queryPage(Query query) {
        Columns columns = Columns.create();
        Page<Role> page = DAO.paginateByColumns(query.getCurrPage(), query.getLimit(), columns, query.getOrderBy());
        for (Role role : page.getList()) {
            Dept dept = deptService.findById(role.getDeptId());
            if (dept != null) {
                role.setDeptName(dept.getName());
            }
        }
        return page;
    }

    @Override
    public boolean saveRole(Role role) {
        boolean tag = save(role)!=null?true:false;
        if (tag) {
            saveRelation(role);
        }
        return tag;
    }

    @Override
    public boolean updateRole(Role role) {
        boolean tag = update(role);
        if (tag) {
            tag = saveRelation(role);
        }
        return tag;
    }

    private boolean saveRelation(Role role) {

        boolean tag = false;
        List<String> deptIdList = role.getDeptIdList();
        if (deptIdList == null) {
            deptIdList = role.get("dept_id_list");
        }
        List<String> menuIdList = role.getMenuIdList();
        if (menuIdList == null) {
            menuIdList = role.get("menu_id_list");
        }
        //根据角色id批量删除，再插入
        roleDeptService.deleteByRoleId(role.getId());
        roleMenuService.deleteByRoleId(role.getId());
        for (Object menuId : menuIdList) {
            tag = roleMenuService.save(new RoleMenu(role.getId(), menuId))==null?false:true;
        }

        for (Object deptId : deptIdList) {
            tag = roleDeptService.save(new RoleDept(role.getId(), deptId))==null?false:true;
        }
        return tag;

    }

    @Override
    public Role findRoleById(Object id) {
        Role role = findById(id);
        Dept dept = deptService.findById(role.getDeptId());
        if (dept != null) {
            role.setDeptName(dept.getName());
        }

        List<RoleDept> roleDeptList = roleDeptService.findListByRoleId(id);
        List<String> deptIdList = new ArrayList<>(roleDeptList.size());
        for (RoleDept roleDept : roleDeptList) {
            deptIdList.add(roleDept.getDeptId());
        }
        role.setDeptIdList(deptIdList);
        List<RoleMenu> roleMenuList = roleMenuService.findListByRoleId(id);
        List<String> menuIdList = new ArrayList<>(roleMenuList.size());
        for (RoleMenu roleMenu : roleMenuList) {
            menuIdList.add(roleMenu.getMenuId());
        }
        role.setMenuIdList(menuIdList);
        return role;
    }

    @Override
    public boolean deleteById(Object... ids){
        boolean tag = false;
        for (Object id : ids) {
            tag = DAO.deleteById(id);
        }
        return tag;
    }
}