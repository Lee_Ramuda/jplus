package io.jplus.admin.service.impl;

import com.jfinal.plugin.activerecord.Page;
import io.jboot.aop.annotation.Bean;
import io.jboot.components.rpc.annotation.RPCBean;
import io.jboot.db.model.Columns;
import io.jboot.service.JbootServiceBase;
import io.jboot.utils.StrUtil;
import io.jplus.admin.model.Dict;
import io.jplus.admin.service.DictService;
import io.jplus.common.Query;

@Bean
@RPCBean
public class DictServiceImpl extends JbootServiceBase<Dict> implements DictService {


    @Override
    public Page<Dict> queryPage(Query query) {
        Columns columns = Columns.create();
        String name = (String) query.get("name");
        if (StrUtil.notBlank(name)) {
            columns.like("name", "%" + name + "%");
        }
        return DAO.paginateByColumns(query.getCurrPage(), query.getLimit(), columns, query.getOrderBy());
    }

    @Override
    public boolean copy(Object id) {
        Dict dict = findById(id);
        dict.setId(null);
        dict.setCode("99");
        int orderNum = dict.getOrderNum() == null ? 0 : dict.getOrderNum();
        dict.setOrderNum(orderNum + 1);
        return StrUtil.isNotBlank(save(dict));
    }

    @Override
    public boolean deleteById(Object... ids) {
        boolean tag = false;
        for (Object id : ids) {
            tag = DAO.deleteById(id);
        }
        return tag;
    }

}