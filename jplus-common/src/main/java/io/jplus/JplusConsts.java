/**
 * Copyright (c) 2017-2018,Retire 吴益峰 (372310383@qq.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.jplus;

public class JplusConsts {

    public static final String SUPER_ADMIN = "1";

    public final static String BASE_VIEW_PATH = "/WEB-INF/view/";

    public final static String JPLUS_USER = "_jplus_user_";

    public final static String JPLUS_USER_ID = "_jplus_user_id_";

    public final static String JWT_TOKEN = "_jplus_jwt_token_";

    public final static String ENCODING = "utf-8";

    public final static String LOGIN_FAIL_USER = "1";

    public final static String LOGIN_FAIL_PWD = "2";

    public final static int USER_STATUS_ENABLE = 1;

    public final static int USER_STATUS_DISABLE = 2;

    public final static int USER_STATUS_DELETE = 3;

    public final static String RET_MSG = "message";

    public final static String RET_CODE = "code";

    public final static String RET_DATA = "data";

    /**
     * 菜单类型
     */
    public enum MenuType {
        /**
         * 目录
         */
        CATALOG(0),
        /**
         * 菜单
         */
        MENU(1),
        /**
         * 按钮
         */
        BUTTON(2);

        private int value;

        MenuType(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }

}
